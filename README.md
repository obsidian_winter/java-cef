由[云中双月](http://cnblogs.com/johness)维护的java-cef版本

分支v49对应chromium49，由于v50之后版本不再支持Windows XP，因此建立v49版本来保留对xp系统的支持
v49已支持maven，请切换到v49分支查看README.md

分支v43对应chromium43，由于v45之后版本不再支持NPAPI，因此建立v43版本来保留对NPAPI的支持

具体的内容参见[Wiki Pages](https://bitbucket.org/Johness/java-cef/wiki/Home)